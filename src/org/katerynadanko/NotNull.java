package org.katerynadanko;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;


@Target(value = ElementType.FIELD)
public @interface NotNull {
     String message() default "shouldn't be null";
}
