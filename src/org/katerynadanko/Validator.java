package org.katerynadanko;

import java.lang.annotation.Annotation;

public interface Validator<T extends Annotation> {
    void validate(Object o) throws ValidationException;

    Class<T> getAnnotationType();
}


//    Сделать аннотацию @Valid, которая имеет ретеншн RUNTIME и вешается на параметр метода
//        Сделать Класс MethodRunner c методом
///**
// * @param clazz класс объекта на котором нужно вызвать метод
// * @param obj обьект класса, или null если метод статический
// * @param methodName имя метода
// * @param paramTypes массив типов параметров метода
// * @param params сами параметры
// * @return возвращаемое методом значение
// */
//public static Object invoke(Class<?> clazz, Object obj, String methodName, Class<?>[] paramTypes, Object... params)
//        Который берет инстанс любого класса и вызывает у него метод с параметрами.
//        Исполняемый код
//        3. Создать аннотацию @NotNull, которая будет вешаться на поле класса. В аннотации @NotNull
//        присутствует метод message(), который возвращает сообщение о неправильном значении поля. Значение по
//        умолчанию - "shouldn't be null" Для параметров метода помеченных аннотацией @Valid делается проверка,
//        и если параметр содержит null-овое поле проаннотированное аннотацией @NotNull выбрасывается проверяемое
//        исключение ValidationException с сообщением, взятом из аннотации.

//public class Main {
//    public static void main(String[] args) {
//        Main m = new Main();
//        Pojo pojo = new Pojo("aaa");
//        Object print = MethodRunner.invoke(Main.class, m, "print", new Class<?>[]{Pojo.class}, pojo);
//
//        System.out.println(print);
//    }
//
//    private void print(@Valid Pojo p) {
//        System.out.println(p);
//    }
//}
//
//public class Pojo {
//
//    @NotNull
//    private String name;
//
//    public Pojo(String name) {
//        this.name = name;
//    }
//}
//
//public interface Validator<T extends Annotation> {
//    void validate(Object o) throws ValidationException;
//
//    Class<T> getAnnotationType();
//}