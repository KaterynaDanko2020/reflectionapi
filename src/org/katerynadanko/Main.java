package org.katerynadanko;

import java.lang.reflect.InvocationTargetException;

public class Main implements Validator {
    public static void main(String[] args) throws NoSuchMethodException, ValidationException, InvocationTargetException, IllegalAccessException {
        Main m = new Main();
        Pojo pojo = new Pojo("aaa");

        Object print =  MethodRunner.invoke(Main.class, m, "print", new Class<?>[]{Pojo.class}, pojo);

        System.out.println(print);

    }
//
private void print (@Valid Pojo p){
        System.out.println(p);
        }

    @Override
    public void validate(Object o) throws ValidationException {

    }

    @Override
    public Class getAnnotationType() {
        return null;
    }
}
