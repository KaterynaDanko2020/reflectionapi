package org.katerynadanko;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class MethodRunner {
//    Class<?> clazz;
//    Object obj;
//    String methodName;
//    Class<?>[] paramTypes;
//    Object params;

    public MethodRunner() {
    }
    public static Object invoke(Class<?> clazz, Object obj, String methodName,
                                Class<?>[] paramTypes, Object... params)
            throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, ValidationException {
        Method method = clazz.getDeclaredMethod(methodName, paramTypes);
        method.setAccessible(true);
        for (int i = 0; i < method.getParameterCount(); i++) {

            if (method.getParameters()[i].isAnnotationPresent(Valid.class)) {

                for (int j = 0; j < params.getClass().getDeclaredFields().length; j++) {
                   params.getClass().getDeclaredFields()[j].setAccessible(true);
                    if (params.getClass().getDeclaredFields()[j].isAnnotationPresent(NotNull.class)||
                           params.getClass().getDeclaredFields()[j].equals(null)) {

                    }
                }
            }
        }
        return method.invoke(obj, params);
    }
}

